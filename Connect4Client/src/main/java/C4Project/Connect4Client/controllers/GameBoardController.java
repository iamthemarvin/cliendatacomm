package C4Project.Connect4Client.controllers;

import java.io.IOException;
import java.net.Socket;
import java.util.Optional;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.rmx.server.PacketManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * This is the gameboardfx controller class that handles the connection of the
 * Application with the server. The gameboardfx controller class implements
 * client sockets (also called just "sockets"). A socket is an endpoint for
 * communication between two machines. The socket that is used in this project
 * is implemented with a client-server TCP based system. The Client will echo to
 * the server a text in this case a packet, that will be received and handle by
 * the server. The server will then echo back a proper response to the packet
 * the client has sent. In this case, a move or communication between the client
 * and the server to run the game Connect 4.
 * 
 * 
 * @author Rafik Ifrani, Xin Ma, Marvin Francisco
 */
public class GameBoardController {

	// As per Ken Fogel's note, log is a better debugging message.
	private final Logger log = LoggerFactory.getLogger(getClass().getName());
	// member variable that handles the conection between this client app to the
	// server
	private Socket socket;
	// final port number that will be used through out the connection
	private final int PORT_NUM = 50000;
	private Button selectedBtn;
	private int selectedCol;
	private int selectedRow;
	private PacketManager pM;
	private boolean gameConnected = false;
	// private member that handles the Client side's validation
	private int[][] gameBoard;
	private Button[] btnList;
	// member variable that stores information that is being sent through the
	// connection
	private byte[] packet;
	private Alert alert;

	@FXML
	private ResourceBundle resources;

	@FXML
	private GridPane mainGrid;

	@FXML
	private Circle circle01, circle11, circle21, circle31, circle41, circle51, circle61;

	@FXML
	private Circle circle02, circle12, circle22, circle32, circle42, circle52, circle62;

	@FXML
	private Circle circle03, circle13, circle23, circle33, circle43, circle53, circle63;

	@FXML
	private Circle circle04, circle14, circle24, circle34, circle44, circle54, circle64;

	@FXML
	private Circle circle05, circle15, circle25, circle35, circle45, circle55, circle65;

	@FXML
	private Circle circle06, circle16, circle26, circle36, circle46, circle56, circle66;

	@FXML
	private Button button0, button1, button2, button3, button4, button5, button6;

	@FXML
	private Circle[][] circle;

	/**
	 * This method is automatically called after the FXML file has been loaded.
	 * Useful if a control must be dynamically configured such as loading data
	 * into a table. Instantiate all the private members that needed default
	 * values.
	 */
	@FXML
	private void initialize() {
		packet = new byte[PacketManager.PACKETSIZE];
		gameBoard = new int[6][7];
		// List of cloumn buttons
		btnList = new Button[] { button0, button1, button2, button3, button4, button5, button6 };
		buttonHoverSetUp();
		// List of circles within the FXML
		circle = new Circle[][] { { circle01, circle11, circle21, circle31, circle41, circle51, circle61 },
				{ circle02, circle12, circle22, circle32, circle42, circle52, circle62 },
				{ circle03, circle13, circle23, circle33, circle43, circle53, circle63 },
				{ circle04, circle14, circle24, circle34, circle44, circle54, circle64 },
				{ circle05, circle15, circle25, circle35, circle45, circle55, circle65 },
				{ circle06, circle16, circle26, circle36, circle46, circle56, circle66 } };

		btnSetUp(true); // Making all column buttons on clickable
		startConnection();
	}

	/**
	 * Called when game is being restarted Resets the color of circles back to
	 * default Resets the gameBoard array and sends a message to the server
	 */
	private void onNewGame() {
		btnSetUp(false);
		// reset the color to default color
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 7; j++) {
				changeColor(i, j, 0);
			}
		}
		gameBoard = new int[6][7];
		makeRequest(PacketManager.START_GAME, -1);

	}

	/**
	 * Takes care of printing an error message related to connection issues
	 */
	private void connectionProblem() {
		showMessage(resources.getString("connectionerror"), 2);
		System.exit(1);
	}

	/**
	 * Takes care of sending a message to the server
	 * 
	 * @param id
	 *            request being made either start game, place move or quit game
	 * @param numb
	 */
	private void makeRequest(int id, int numb) {
		try {
			pM.packetSender(id, numb, -1,-1);
		} catch (IOException e) {

			connectionProblem();
		}
		try {
			readReturn();
		} catch (IOException e) {
			System.exit(0);
		}
	}

	/***
	 * Takes care of setting up the events for mouse enter and mouse leave on
	 * each button.
	 */
	private void buttonHoverSetUp() {
		for (Button b : btnList) {
			b.setOnMouseEntered(e -> onMouseEntered(e));
			b.setOnMouseExited(e -> onMouseLeft(e));
		}
	}

	/***
	 * Event handler to change the opacity of a column button When hovering over
	 * it.
	 * 
	 * @param e
	 *            button mouse if over
	 */
	private void onMouseEntered(MouseEvent e) {
		Button btn = (Button) e.getSource();
		btn.setOpacity(0.30);
	}

	/***
	 * Event handler to change the opacity of a column button Back to 0 when
	 * leaving it.
	 * 
	 * @param e
	 *            button mouse just left
	 */
	private void onMouseLeft(MouseEvent e) {

		Button btn = (Button) e.getSource();
		btn.setOpacity(0);
	}

	/**
	 * The event handler registered in the FXML file for when the button is
	 * pressed. Allowing us to know which column was clicked Sends selected
	 * column to server for validation
	 * 
	 * @param event
	 * @throws IOException
	 */
	@FXML
	void handleButtonAction(ActionEvent event) {
		selectedBtn = (Button) event.getSource(); // identify which button was
		String id = selectedBtn.getId();
		selectedCol = Integer.parseInt(id.substring(6, 7));// Getting the col of
															// button

		makeRequest(PacketManager.PLACE_MOVE, selectedCol);

	}

	/**
	 * This method will start the connection of the client and server app when
	 * the proper requirements have been met in making a socket connection
	 * HostName and Port Number.
	 */
	private void startConnection() {
		String hostName = displayInstruction(resources.getString("message"));
		int fiveFail = 5;
		while (!gameConnected)
			try {
				socket = new Socket(hostName, PORT_NUM);
				pM = new PacketManager(socket);
				makeRequest(PacketManager.START_GAME, -1);
			} catch (Exception e) {
				if (fiveFail == 0) {
					log.info("Server is not ready, try again later.");
					showMessage(resources.getString("fivetry"), 1);
					System.exit(0);
				} else {
					hostName = displayInstruction(resources.getString("re-enter"));
				}
				fiveFail--;
			}
	}

	/**
	 * Method takes care of identifying which type of message was sent back from
	 * the server. Will take appropriate measures depending on which
	 * 
	 * @throws IOException
	 */
	private void readReturn() throws IOException {

		packet = pM.packetReceiver();

		switch (packet[0]) {
		case PacketManager.START_GAME:
			btnSetUp(false);
			gameConnected = true;
			break;
		case PacketManager.PLACE_MOVE:
			placeMove();
			break;
		case PacketManager.BAD_MOVE: // Do nothing
			break;
		case PacketManager.GAME_WON:
			displayWin();
			break;
		case PacketManager.GAME_LOST:
			placeMove();
			displayMessage("You have Lost!", "Do you wish to restart?");
			break;
		case PacketManager.GAME_DRAW:
			placeMove();
			displayMessage("The Game has ended in a draw!", "Do you wish to restart?");
			break;
		}
	}

	/***
	 * Method that places the clients move, after being validated by the server.
	 * Will also place the Ai's move and set the selected button the the column
	 * the Ai picked
	 */
	private void placeMove() {
		selectedRow = packet[1];
		isColumnFull(1);// Checking if after adding token the column is full
		changeColor(selectedRow, selectedCol, 1);

		selectedRow = packet[2]; // Setting Ai's row
		selectedCol = packet[3]; // Setting Ai's column
		int id;
		for (Button btn : btnList) {
			// Getting column number off btn name (ie Button0)
			id = Integer.parseInt(btn.getId().substring(6, 7));
			if (selectedCol == id)
				selectedBtn = btn; // Setting selectedBtn to Ai's column to
									// disable if full
		}
		isColumnFull(2);
		changeColor(selectedRow, selectedCol, 2);
	}

	
	/**
	 * This method will take care of setting the token on the board 
	 * and disable the button of a column that is full
	 */
	private void isColumnFull(int move) {
				
		gameBoard[selectedRow][selectedCol] = move;
		if (selectedRow == 0)
					selectedBtn.setDisable(true);
	}

	/**
	 * Displays a message after the game ended in a win, loss or draw. Will ask
	 * user if they want to restart Clicking yes will refresh the board,
	 * clicking no will exit the game
	 * 
	 * @param msg
	 *            Header Message
	 * @param choice
	 *            Content text
	 * @throws IOException
	 */
	private void displayMessage(String msg, String choice) {
		alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Dialog");
		alert.setHeaderText(msg);
		alert.setContentText(choice);
		
		
		ButtonType btnYes = new ButtonType("Yes"); // Making a button labeled
													// Yes
		ButtonType btnNo = new ButtonType("No",ButtonData.CANCEL_CLOSE); // Making a button labeled No

		alert.getButtonTypes().setAll(btnYes, btnNo);// Adding both buttons to
													// the alert box	
		// Getting which button was pressed
		Optional<ButtonType> result = alert.showAndWait();

		if (result.get() == btnYes)
			onNewGame(); // Restarting game

		else if (result.get() == btnNo)
			onExitGame(); // Exiting game
	}		

	/**
	 * Takes care of notifying the server that the user has quit Closes the
	 * socket and closes the program
	 */
	public void onExitGame() {
		// Notifying server game has been quit
		makeRequest(PacketManager.QUIT_GAME, -1);
		try {
			socket.close();
		} catch (IOException e) {
			System.exit(0);
		}
		System.exit(0);
	}

	/**
	 * Takes care of setting the clients winning move Will call method
	 * displaying a winning message for the user
	 * 
	 * @throws IOException
	 */
	private void displayWin() throws IOException {
		selectedRow = packet[1];
		isColumnFull(1);
		changeColor(selectedRow, selectedCol, 1);
		displayMessage("You have Won!", "Do you wish to restart?");
	}

	/**
	 * First message box user is show when launching game Asking the user to
	 * enter an ip to connect to the server to play the game
	 * 
	 * @param message
	 * @return
	 */
	private String displayInstruction(String message) {
		String ip;
		TextInputDialog dialog = new TextInputDialog(resources.getString("ip"));
		dialog.setTitle(resources.getString("startGame"));
		dialog.setHeaderText(message);

		dialog.setContentText(resources.getString("enterIp"));

		// ButtonType btnCancel = new ButtonType("Cancel"); // Making a button
		// labeled
		// Getting which button was pressed
		Optional<String> result = dialog.showAndWait();
		if (!result.isPresent())
			System.exit(0);

		// receive IP then send it back to the connecting server
		ip = result.get().toString();
		ip = ip.replace(']', ' ');
		ip = ip.substring(ip.indexOf('[') + 1).trim();

		return ip;
	}

	/**
	 * 
	 * Takes care of changing the color of the circles Depending on a code will
	 * set it red, black or default(blue)
	 * 
	 * @param row
	 *            Row of token
	 * @param col
	 *            Column of token
	 * @param code
	 *            The player's token
	 */
	private void changeColor(int row, int col, int token) {
		switch (token) {
		case 1: // Player token
			circle[row][col].setFill(Color.RED);
			break;
		case 2: // Ai token
			circle[row][col].setFill(Color.BLACK);
			break;
		default: // setting to default color
			circle[row][col].setFill(Color.WHITE);
			break;
		}
	}

	/***
	 * Takes care of enabling or disabling all the column buttons Used when game
	 * ends or game starts
	 * 
	 * @param locked
	 *            True to disable, false to enable
	 */
	private void btnSetUp(boolean locked) {
		for (Button btn : btnList)
			btn.setDisable(locked);
	}

	/**
	 * This method will display a proper message to the user reaching a number
	 * of 5 tries in connecting to the server.
	 * 
	 * @param message 
	 * @param choice
	 * 
	 */
	public void showMessage(String message, int choice) {
		Alert alert;
		if (choice == 1) {
			alert = new Alert(AlertType.WARNING);
			alert.setHeaderText("Alert");
			alert.setContentText(message);

		} else {
			alert = new Alert(AlertType.ERROR);
			alert.setHeaderText("Error");
			alert.setContentText(message);
		}
		alert.setTitle("Something went wrong.");
		alert.showAndWait();

	}
}
